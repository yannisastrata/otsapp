import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {KeycloakService} from 'keycloak-angular';
import {AuthService} from './services/auth.service';
import KcAdminClient from 'keycloak-admin';

const kcAdminClient = new KcAdminClient({
  baseUrl: 'http://nl-svr-bda1:9595/auth'
});

describe('AppComponent', () => {
  let as: AuthService;
  beforeEach(async(() => {
    as = new AuthService(new KeycloakService());
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        KeycloakService, AuthService
      ]
    }).compileComponents();
  }));
  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
