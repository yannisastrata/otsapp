import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppAuthGuard} from './AppAuthGuard';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {UserComponent} from './components/user/user.component';
import {AdminComponent} from './components/admin/admin.component';
import {UpdateuserComponent} from './components/updateuser/updateuser.component';
import {CreateUserComponent} from './components/createuser/create-user.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { roles: ['user'], title: 'Dashboard'}
  },
  {
    path: 'admin',
    component: AdminComponent,
    data: { title: 'Admin'}
  },
  {
    path: 'user',
    component: UserComponent,
    data: { roles: ['user'], title: 'User'}
  },
  {
    path: 'updateuser',
    component: UpdateuserComponent,
    data: { roles: ['admin'], title: 'Update'}
  },
  {
    path: 'createuser',
    component: CreateUserComponent,
    data: { roles: ['admin'], title: 'Dashboard'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class AppRoutingModule { }
