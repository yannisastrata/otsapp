import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../model/user';
import {AdminService} from '../../services/admin.service';
import {ActivatedRoute, Router} from '@angular/router';
import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation';

@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.scss']
})
export class UpdateuserComponent implements OnInit {
  user: User;
  constructor(private admin: AdminService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.user = new User();
    this.user.id = this.route.snapshot.paramMap.get('userId');
    this.user.username = this.route.snapshot.paramMap.get('username');
    this.user.firstName = this.route.snapshot.paramMap.get('firstname');
    this.user.lastName = this.route.snapshot.paramMap.get('lastname');
    this.user.email = this.route.snapshot.paramMap.get('email');
  }
  updateUser(oldU: User) {
    this.admin.updateUser(oldU, oldU);
    this.router.navigateByUrl('admin');
  }
}
