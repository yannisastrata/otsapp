import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateuserComponent } from './updateuser.component';
import {FormsModule} from '@angular/forms';
import {AdminService} from '../../services/admin.service';
import {AuthService} from '../../services/auth.service';
import {KeycloakService} from 'keycloak-angular';
import {RouterTestingModule} from '@angular/router/testing';

describe('UpdateuserComponent', () => {
  let component: UpdateuserComponent;
  let fixture: ComponentFixture<UpdateuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateuserComponent ],
      imports: [FormsModule, RouterTestingModule],
      providers: [AdminService, AuthService, {provide: KeycloakService, useValue: new KeycloakService()}]
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    this.keycloakAngular.login();
    fixture = TestBed.createComponent(UpdateuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
