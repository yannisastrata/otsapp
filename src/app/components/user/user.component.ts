import { Component, OnInit } from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {Keycloak} from 'keycloak-angular/lib/core/services/keycloak.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  username: string;
  user: any;
  roles: string[];
  profile: Keycloak.KeycloakProfile;
  uname: string;
  firstname: string; lastname: string; verified: boolean;

  constructor(private keycloakAngular: KeycloakService) {
  }

  ngOnInit() {
    this.getUserDetails();
  }

  getUserDetails() {
    const keycloakInstance = this.keycloakAngular.getKeycloakInstance();
    this.username = keycloakInstance.profile.username;
    this.uname = keycloakInstance.profile.username;
    this.firstname = keycloakInstance.profile.firstName;
    this.lastname = keycloakInstance.profile.lastName;
    this.verified = keycloakInstance.profile.emailVerified;
    this.roles = keycloakInstance.realmAccess.roles;
  }
  changeProfile() {
    this.keycloakAngular.getKeycloakInstance().accountManagement();
  }

  getToken(): string {
    return this.keycloakAngular.getKeycloakInstance().token;
  }
}
