import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user.component';
import {AdminService} from '../../services/admin.service';
import {AuthService} from '../../services/auth.service';
import {KeycloakService} from 'keycloak-angular';
import {Keycloak} from 'keycloak-angular/lib/core/services/keycloak.service';
import {AppModule} from '../../app.module';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  let Mock1 = {
    getUserDetails: jasmine.createSpy('getUserDetails')
};
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [AdminService, AuthService,  {provide: KeycloakService, useValue: new KeycloakService()},  { provide: KeycloakService, useValue: Mock1 }],
      declarations: [ UserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    let kservice: KeycloakService;
    kservice = new KeycloakService();
    const keycloakInstance = kservice.getKeycloakInstance();
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
