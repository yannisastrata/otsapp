import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user';
import {AdminService} from '../../services/admin.service';
import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation';
import {el} from '@angular/platform-browser/testing/src/browser_util';
import CredentialRepresentation from 'keycloak-admin/lib/defs/credentialRepresentation';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
user: User;
created: string;
unameExists: string; mailExists: string;
shouldcreate: boolean;
  constructor(private admin: AdminService) {
    this.user = new User();
  }

  ngOnInit() {
    this.shouldcreate = false;
  }

  createUser() {
    this.checkFields();
    this.admin.findUser(this.user).then(res => {
      console.log(this.shouldcreate);
      if (!this.shouldcreate) {
        this.created = 'User already exists';
        setTimeout(() => {
          location.reload();
        }, 2000);
      } else {
        this.admin.createUser(this.user);
        this.created = 'User ' + this.user.username + ' created';
        setTimeout(() => {
          location.reload();
        }, 2000);
      }
    });
  }

  checkFields() {
    this.admin.findUserByUsername(this.user.username).then(res => {
      console.log(res);
      if (res != null) {
        this.unameExists = 'Username already exists';
        this.shouldcreate = false;
      } else {
        this.unameExists = '';
        this.shouldcreate = true;
      }
    });
    this.admin.findUserByEmail(this.user.email).then(res => {
      console.log(res);
      if (res != null) {
        this.mailExists = 'Email already exists';
        this.shouldcreate = false;
      } else {
        this.mailExists = '';
        this.shouldcreate = true;
      }
    });
  }
}
