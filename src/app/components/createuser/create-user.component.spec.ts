import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUserComponent } from './create-user.component';
import {FormsModule} from '@angular/forms';
import {KeycloakService} from 'keycloak-angular';
import {Admin} from 'typeorm';
import {AdminService} from '../../services/admin.service';
import {AuthService} from '../../services/auth.service';
import {KeycloakAdminClient} from 'keycloak-admin/lib/client';

describe('CreateUserComponent', () => {
  let component: CreateUserComponent;
  let fixture: ComponentFixture<CreateUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUserComponent ],
      imports: [FormsModule],
      providers: [AdminService, AuthService, {provide: KeycloakService, useValue: new KeycloakService()}]
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(CreateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
