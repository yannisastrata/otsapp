import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardComponent } from './dashboard.component';
import {DataTablesModule} from 'angular-datatables';
import * as jquery from 'jquery';
import {AdminService} from '../../services/admin.service';
import {KeycloakService} from 'keycloak-angular';
declare var $;
import {KeycloakAdminClient} from 'keycloak-admin/lib/client';

describe('DashboardComponent', async () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let kcAdminClient: KeycloakAdminClient;
  kcAdminClient = new KeycloakAdminClient();
  await this.kcAdminClient.auth({
    username: 'admin',
    password: 'password',
    grantType: 'password',
    clientId: 'admin-cli',
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports: [ DataTablesModule ],
      providers: [ {provide: KeycloakService, useValue: new KeycloakService()} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
