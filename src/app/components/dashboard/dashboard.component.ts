import {Component, OnInit, ViewChild} from '@angular/core';
declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  constructor() {
  }
  ngOnInit(): void {
    this.dtOptions = {
      ajax: {
        url: 'http://localhost:3000/temps',
        type: 'GET',
        dataSrc: ''
      },
      columns : [
        {
          title: 'fleetvisorid',
          data: 'fleetvisorid'
        },
        {
          title: 'thermokingid',
          data: 'thermokingid'
        },
        {
          title: 'astratacid',
          data: 'astratacid'
        }
      ]
    };
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }
}
