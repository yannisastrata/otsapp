import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AdminService} from '../../services/admin.service';
import {User} from '../../model/user';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger =  new Subject();
  users: User[] = new Array();
  user: User;

  constructor(private adminService: AdminService) {

  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.adminService.findUsers()
      .then(result => {
      this.users = result;
      this.dtTrigger.next();
    });
   }
  deleteUser(user: User) {
    if (confirm('Are you sure you want to delete ' + user.username + '?')) {
      this.adminService.deleteUser(user);
    }
  }
}
