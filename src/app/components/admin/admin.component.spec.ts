import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminComponent } from './admin.component';
import {DataTablesModule} from 'angular-datatables';
import {RouterTestingModule} from '@angular/router/testing';
import {KeycloakService} from 'keycloak-angular';
import {AdminService} from '../../services/admin.service';
import {AuthService} from '../../services/auth.service';
import {AppModule} from '../../app.module';

describe('AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComponent ], imports: [ DataTablesModule, RouterTestingModule ],
      providers: [ AdminService, AuthService, {provide: KeycloakService, useValue: new KeycloakService()} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // it('should be filled', () => {
  //   service.findUsers().then(
  //     value => expect(value).not.toBeNull()
  //   );
  // });
});
