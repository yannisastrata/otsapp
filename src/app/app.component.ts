import { Component } from '@angular/core';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  username: string;

  constructor(private authservice: AuthService) {
  }
  logout() {
    this.authservice.logout();
  }
  isLoggedIn(): boolean {
    return this.authservice.isLoggedIn();
  }
  userIsAdmin(): boolean {
    return this.authservice.userIsAdmin();
  }
}
