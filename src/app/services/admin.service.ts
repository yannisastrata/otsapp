import { Injectable } from '@angular/core';
import KcAdminClient from 'keycloak-admin';
import {AuthService} from './auth.service';
import {User} from '../model/user';
import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation';
import {KeycloakService} from 'keycloak-angular';
import {root} from 'rxjs/internal-compatibility';

const kcAdminClient = new KcAdminClient({
  baseUrl: 'http://nl-svr-bda1:9595/auth'
});

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(protected keycloakAngular: KeycloakService) {
    kcAdminClient.auth({
      username: 'admin',
      password: 'password',
      grantType: 'password',
      clientId: 'admin-cli',
    });
    keycloakAngular.getToken().then(token => {
      if (token != null) {
        kcAdminClient.setAccessToken(token);
      }
    }).catch(() => console.log('error'));
    // console.log(keycloakAngular.getKeycloakInstance().token);
    // kcAdminClient.setAccessToken(keycloakAngular.getKeycloakInstance().token);
  }
  users = [];

  static convertToUser(userRepresentation): User {
    const user = new User();
    user.id = userRepresentation.id;
    user.createdTimestamp = userRepresentation.createdTimestamp;
    user.username = userRepresentation.username;
    user.email = userRepresentation.email;
    user.emailVerified = userRepresentation.emailVerified;
    user.firstName = userRepresentation.firstName;
    user.lastName = userRepresentation.lastName;
    return user;
  }
  findUsers(): Promise<User[]> {
    return kcAdminClient.users.find().then(
      result => {
        console.log(result);
        return result.map(repr => AdminService.convertToUser(repr));
      }
    );
  }
  findUser(user: User): Promise<UserRepresentation> {
    return kcAdminClient.users.findOne(user);
  }

  findUserByUsername(username: string): Promise<User> {
    return this.findUsers().then(users => {
      return users.find(u => u.username === username);
    });
  }
  findUserByEmail(mail: string): Promise<User> {
    return this.findUsers().then(users => {
      return users.find(u => u.email === mail);
    });
  }
  deleteUser(user: User) {
    kcAdminClient.users.del(user);
    location.reload();
  }
  updateUser(old: User, newU: User) {
    kcAdminClient.users.update(old, newU);
  }
  createUser(user: User) {
    kcAdminClient.users.create(user);
  }
}
