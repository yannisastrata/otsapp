import { TestBed } from '@angular/core/testing';

import { AdminService } from './admin.service';
import {KeycloakService} from 'keycloak-angular';
import {of} from 'rxjs/internal/observable/of';

class MockKey {
  sendMail() {
    return of(new KeycloakService());
  }
}

let mockKey;

describe('AdminService', () => {
  let service: AdminService;
  mockKey = new MockKey();
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ {provide: KeycloakService, useValue: new KeycloakService()} ]
  }));

  it('should be created', () => {
    service = TestBed.get(AdminService);
    expect(service).toBeTruthy();
  });
});
