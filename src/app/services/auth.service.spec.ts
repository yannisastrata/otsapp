import { TestBed } from '@angular/core/testing';
import { async } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {KeycloakService} from 'keycloak-angular';

describe('AuthService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [KeycloakService]
    });
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });
});
