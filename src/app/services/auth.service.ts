import { Injectable } from '@angular/core';
import {KeycloakService} from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(protected keycloakAngular: KeycloakService) {
  }
  logout() {
    this.keycloakAngular.logout('http://localhost:4200');
  }
  isLoggedIn(): boolean {
   return this.keycloakAngular.getToken() != null;
  }
  userIsAdmin(): boolean {
    return this.keycloakAngular.isUserInRole('admin');
  }
  getToken(): Promise<string> {
    return this.keycloakAngular.getToken();
  }
}
