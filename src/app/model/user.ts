import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation';
import CredentialRepresentation from 'keycloak-admin/lib/defs/credentialRepresentation';

export class User implements UserRepresentation {
  createdTimestamp: number;
  email: string;
  emailVerified: boolean;
  enabled: boolean;
  firstName: string;
  id: string;
  lastName: string;
  username: string;
  name: string;
  credentials: CredentialRepresentation[];
  // id: string;
  // // createdTimestamp: string;
  // username: string;
  // email: string;
  // firstName: string;
  // lastName: string;
  // enabled: boolean;
  // verified: boolean;
  // name: string;
  //
  //
  // constructor(username?: string, email?: string, firstName?: string, lastName?: string, enabled?: boolean, verified?: boolean) {
  //   this.username = username;
  //   this.email = email;
  //   this.firstName = firstName;
  //   this.lastName = lastName;
  //   this.enabled = enabled;
  //   this.verified = verified;
  // }

}
